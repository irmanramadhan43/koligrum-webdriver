import {Given, When, Then} from "@cucumber/cucumber";
import homePage from '../pageobjects/demoblaze/demo-home-page'



Then (/^User should see home page demoblaze$/, () => {
  homePage.assertLogoutLinkText();
})

