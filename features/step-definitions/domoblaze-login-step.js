import {Given, When, Then} from "@cucumber/cucumber";
import loginPage from '../pageobjects/demoblaze/demo-blaze-login-page'


Given (/^User is on demoblaze login page$/, () => {
  loginPage.open()
})

When (/^User is succesfully logged in to demoblaze$/, () => {
  loginPage.clickLinkTextLogin();
  loginPage.inputUsername('irman');
  loginPage.inputPassword('12345678');
  loginPage.clickBtnLogin();
})

